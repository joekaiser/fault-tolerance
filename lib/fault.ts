export class Fault extends Error {
  private obj: any = null;
  public metadata: any = {};
  private _id: string;

  public get id() {
    return this._id;
  }

  constructor(data: any, context?: any, preProcess?: (context: any) => {}) {
    super(data);
    this._id = (Math.random() + Date.now()).toString(36)
    if (preProcess && context) {
      context = preProcess(context);
    }

    let metadata: any = {}
    metadata = (context || {});
    if (typeof metadata != 'object') {
      metadata = { value: metadata };
    }

    if (!data) {
      data = "Internal fault-tolerance message. No data was specified";
    }
    if (data instanceof Fault) {
      this.mergeFromFault(data, metadata);
    }
    else if (data instanceof Error) {
      this.mergeFromError(data, metadata);
    }
    else {
      this.initialize(data, metadata);
    }

  }

  public toString() {
    let result = `${this.stack}\nid: ${this._id}`;
    if (this.metadata) {
      result = `${result}\nmetadata:\n${JSON.stringify(this.metadata, null, 2)}`;
    }
    return result;
  }

  public get log(): string {
    return this.toString();
  }

  public toObject() {
    if (this.obj == null) {

      this.obj = {
        message: this.message,
        id:this._id,
        stack: [],
        metadata: this.metadata
      };

      if (this.stack) {
        let st = this.stack.split('\n');
        st.forEach(s => {
          this.obj.stack.push(s.trim());
        });
      }
    }
    return this.obj;
  }

  public toJSON() {
    return JSON.stringify(this.toObject());
  }

  private initialize(data: any, context: any) {
    if (Array.isArray(data)) {
      context.data = data;
    } else if (typeof data == 'object') {
      this.message = (data.message || "");
      context = { ...context, ...data };
    }

    this.metadata = context
  }

  private mergeFromError(error: Error, context: object) {
    this.message = error.message;

    let thisStack: string[] = [];
    let thatStack: string[] = [];

    if (this.stack) {
      thisStack = this.stack.split('\n');
      thisStack.splice(0, 1);
    }

    if (error.stack) {
      thatStack = error.stack.split('\n');
      thatStack.splice(0, 1);
    }

    thisStack = thisStack.concat(thatStack);
    this.stack = `Error: ${this.message}\n${thisStack.join('\n')}`;

    this.metadata = { ...context };
  }

  private mergeFromFault(error: Fault, context: object) {
    this.stack = error.stack;
    this.message = error.message;
    this.metadata = { ...(error.metadata || {}), ...context };
  }
}
