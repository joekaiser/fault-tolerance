### Changelog

All notable changes to this project will be documented in this file. Dates are displayed in UTC.

Generated by [`auto-changelog`](https://github.com/CookPete/auto-changelog).

#### [v1.3.1](https://gitlab.com/joekaiser/fault-tolerance/compare/v1.3.0...v1.3.1)

> 1 November 2019

- putting this on hold [`b4ae323`](https://gitlab.com/joekaiser/fault-tolerance/commit/b4ae3236ebfbf38753d675fa92fbc0e7d8a98c16)
- add ci/cd [`6af684b`](https://gitlab.com/joekaiser/fault-tolerance/commit/6af684be57241b498a2fb7bc5c22b28d01bef22d)
- add yml back in [`6df3fc9`](https://gitlab.com/joekaiser/fault-tolerance/commit/6df3fc98422106b91c6610235b1f5cb4c45115ce)

#### [v1.3.0](https://gitlab.com/joekaiser/fault-tolerance/compare/v1.2.0...v1.3.0)

> 5 September 2019

- add a unique id to all errors [`84d7ae3`](https://gitlab.com/joekaiser/fault-tolerance/commit/84d7ae3dc7c19309616578ac27760a0aab8a6b46)
- update changleog [`90585ce`](https://gitlab.com/joekaiser/fault-tolerance/commit/90585ceaeb08e7c73acc47da4b641c169815706b)

#### [v1.2.0](https://gitlab.com/joekaiser/fault-tolerance/compare/v1.1.0...v1.2.0)

> 5 July 2019

#### [v1.1.0](https://gitlab.com/joekaiser/fault-tolerance/compare/v1.0.0...v1.1.0)

> 5 July 2019

- append pre stack to current when wrapping an error [`987b54d`](https://gitlab.com/joekaiser/fault-tolerance/commit/987b54de2a51dfee9a85ecbf81ba4304291cad09)
- updated changelog [`e98b141`](https://gitlab.com/joekaiser/fault-tolerance/commit/e98b141a543213124a4bfd6966a75f823348ddfc)
- Update README.md [`a17b0d3`](https://gitlab.com/joekaiser/fault-tolerance/commit/a17b0d3b97ce8b7d540ce7a3e65318f8e34c1cd8)

### [v1.0.0](https://gitlab.com/joekaiser/fault-tolerance/compare/v0.1.1...v1.0.0)

> 17 April 2019

- add preprocessor. fixes #1 [`#1`](https://gitlab.com/joekaiser/fault-tolerance/issues/1)
- cleanup package creation [`642d7e5`](https://gitlab.com/joekaiser/fault-tolerance/commit/642d7e51430f7b6ca2e29d72101527724cc89117)
- Add example for preProcessor [`1074d39`](https://gitlab.com/joekaiser/fault-tolerance/commit/1074d392c5ae86faf92b19f4e57045391e612457)
- add keywords [`1b04333`](https://gitlab.com/joekaiser/fault-tolerance/commit/1b043332a11fbec3b29f2d0ca11187a47b679971)

#### [v0.1.1](https://gitlab.com/joekaiser/fault-tolerance/compare/v0.1.0...v0.1.1)

> 17 April 2019

- update examples [`314641e`](https://gitlab.com/joekaiser/fault-tolerance/commit/314641eb1bff0cc8f8c15eb461c57afc49b0c8bd)
- Update README.md [`3d02acc`](https://gitlab.com/joekaiser/fault-tolerance/commit/3d02acca3db9c521960cd8cfc2926a081c83ec9a)
- Update README.md [`a8f831d`](https://gitlab.com/joekaiser/fault-tolerance/commit/a8f831d84cdea70b9c2a56a085b998cce0a8cbc2)

#### v0.1.0

> 16 April 2019

- first! [`78e307f`](https://gitlab.com/joekaiser/fault-tolerance/commit/78e307f3c2086ff82c887d6ef3ed44aa060037f3)
- remove lodash and simplify api [`dcc1ebe`](https://gitlab.com/joekaiser/fault-tolerance/commit/dcc1ebe715e599a3bd5e54101c05a633461d74bf)
- add readme [`2b3b5ac`](https://gitlab.com/joekaiser/fault-tolerance/commit/2b3b5aced5ad37604e6685ffe8b954df58dbee57)
