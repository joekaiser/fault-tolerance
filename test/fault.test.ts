/// <reference types='jest' />


import { Fault } from '../lib/fault';



describe('Simple Error', () => {
  test('Has an ID', () => {
    const data = 'Some message here';
    let error = new Fault(data);
    expect(error.id).toBeTruthy();
  });

  test('Has NO metadata', () => {
    const data = 'Some message here';
    let error = new Fault(data);
    expect(error.message).toBe(data);
  });

  test('Has a metadata context', () => {
    const data = 'Some message here';
    const context = 'context';
    let error = new Fault(data, context);
    expect(error.log).toContain(context);
  });


  test('metadata can contain an object', () => {
    const data = 'Some message here';
    const context = { k: 'v' };
    let error = new Fault(data, context);
    expect(error.log).toContain(context.k);

  });

  test('has a log getter', () => {
    const data = "message";
    let error = new Fault(data);
    expect(error.log).toBeDefined();
  });

  test('has a toJson', () => {
    const data = "message";
    const context = "my context";
    let error = new Fault(data, context);
    let json = error.toJSON();
    expect(json).toContain('{"message":"message","id"');
    expect(json).toContain('"metadata":{"value":"my context"}');
  });

  test('has a toObject', () => {
    const data = 'my message';
    const context = { prop: 1 };
    let error = new Fault(data, context);
    let errObj = error.toObject();
    expect(errObj.id).toBeTruthy();
    expect(errObj.message).toBe(data);
    expect(errObj.stack).toBeTruthy();
    expect(errObj.stack.length).toBeGreaterThan(0);
    expect(errObj.metadata).toBeTruthy();
    expect(errObj.metadata.prop).toBe(context.prop);
  });


  test('accepts an array of data', () => {
    const data = ['one', 2, false];
    let error = new Fault(data);
    expect(error.log).toContain('one');
    expect(error.log).toContain(2);
    expect(error.log).toContain(false);
  });


  test('message is copied over from data object', () => {
    const data = { message: 'test', value: true };
    let error = new Fault(data);
    expect(error.message).toBe(data.message);
  });

  test('data object and context object', () => {
    const data = { message: 'test', prop1: 2 };
    const context = { prop2: 1 };
    let error = new Fault(data, context);
    expect(error.metadata.prop1).toBeDefined();
    expect(error.metadata.prop2).toBeDefined();
  });

  test('data array and context object', () => {
    const data = ['one', 2, false];
    const context = { prop: 1 };
    let error = new Fault(data, context);
    expect(error.metadata.data).toBe(data);
    expect(error.metadata.prop).toBe(context.prop);
  });

  test('it can wrap an error', () => {
    let error: any = null;
    const context = { moreContext: true };
    let test = () => {
      throw new Error('something bad');
    }
    try {
      test();
    }
    catch (e) {
      error = new Fault(e, context);
    }
    expect(error.toString().indexOf('at test (')).toBeGreaterThan(0);
    expect(error.metadata).toEqual(context);
  });

  test('can preprocess context', () => {
    const pre = (context: any) => {
      delete context.secret;
      return context;
    };

    const data = "preprocess test";
    const context = { prop: 1, secret: 'test' };
    let error = new Fault(data, context, pre);
    expect(error.metadata.prop).toBe(context.prop);
    expect(error.metadata.secret).toBeUndefined();
  });
});
