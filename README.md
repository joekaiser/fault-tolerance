[![pipeline status](https://gitlab.com/joekaiser/fault-tolerance/badges/master/pipeline.svg)](https://gitlab.com/joekaiser/fault-tolerance)

*This project isn't dead; it is feature complete and stable.*

fault-tolerance
===============

## Process and normalize errors

This module offers a simple *Fault* class that will normalize the output of an error. Additional context can be associated and included in the *toString()* making analyzing what went wrong easier. 

### Installation

Available as an [NPM Package](https://www.npmjs.com/package/fault-tolerance)
`$ npm install fault-tolerance`

### Details

`Fault.id`
Get a unique identifier for the error

`Fault.log`
Get the formatted output

`Fault.toObject()`
Get an object representing the formatted output

`Fault.toJson()`
Get a JSON formatted version of the output. The Stacktrack will be split into an array.

`constructor(data, context, preProcess)`
Create a new Fault using the given data and context. Aditionally you an pre-process the context before it is written to the Fault. See examples below

### Example

[See on RunKit](https://runkit.com/joekaiser/runkit-fault-tolerance)

The *Fault* class extends *Error* which means at the most basic use-case is `throw new Fault('Something bad happened');`

More often than not you need additional context in an error.

```javascript
let context = {
  prop1: 'value',
  prop2: false
}

throw new Fault('Nothing to see here', context);
```

the output from that would be
```Plain Text
Error: Nothing to see here
  at ......
  at ......
  at ......
id: k06varft.9y6
metadata:
{
  "prop1": "value",
  "prop2": false
}
```

There are a few intricacies of the *data* and *context* params. If the data is a string then that will become the message of the error. However, if the data is an object or array it will be merged into the context. If the data object has a *message* property then it will be used as the message for the error.

```javascript
const data = { message: 'test', prop: 2 };
const context = { prop: 1 };
let error = new Fault(data, context);
```

Output:
```Plain Text
Error: test
  at ......
  at ......
  at ......
id: k06varft.9y6
metadata:
{
  "prop2": 1,
  "message": "test",
  "prop1": 2
}
```

A real-world usecase of this is when you are using some 3rd party API and you want some request context in the thrown error. Note that even though you are throwing a new error the original stack trace is entact.

```javascript
function checkWeather(location){
  try {
    let result = weatherAPI.getForecast(location);
  }
  catch(e) {
    e = new Fault(e, {location:location});
    logger.log(e.log);
    throw e;
  }
}
```

Or if you just wanted to cleanly include additional context in any error you throw

```javascript
function checkpoint(droids:[]){
  try{
    droids.forEach(d => {
      if(d.isWanted) {
        if(jediIsPresent) {
          throw new Error('These are not the droids you are looking for');
        }
        detain(d);
      }
    })
  }
  catch(e) {
    // a Fault will preserve the original stacktrace
    throw new Fault(e, {droids: droids});
  }
}
```

The output from that would look like:
```Plain Text
Error: These are not the droids you are looking for
  at ......
  at ......
  at ......
id: k06varft.9y6
metadata:
{
  "droids": [
    {
      "name": "R2D2",
      "isWanted": true
    },
    {
      "name": "C3PO",
      "isWanted": true
    }
  ]
}
```

Sometimes you will need to make sure sensitive information isn't written to the logs. Add a pre-Processor to the constructor to ensure the context is how you want it.

```javascript
function redactUserInformation(context){
  delete context.address;
  delete context.phone;
  delete context.emailAddress;
  return context;
}

// user:{id, address, phone, emailAddress }
function processClaim(user){
  try{
    claims.proccess(user);
  }
  catch(e){
    throw new Fault(e, user, redactUserInformation);
  }
}
```

```Plain Text
Error: Error while processing claim
  at ......
  at ......
  at ......
id: k06varft.9y6
metadata:
{
  "id": "17a876df0b"
}
```
